<?php

namespace ProfideoLeagueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ProfideoLeagueBundle\Repository\SeasonRepository")
 * @ORM\Table(name="season")
 */
class Season
{
    //##################################################
    //general
    //##################################################

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false, options={"default"= ""})
     */
    private $name;

    /**
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    private $number = 0;

    /**
     * @ORM\OneToMany(targetEntity="ProfideoLeagueBundle\Entity\Game", mappedBy="season", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    private $games;

    /**
     * @var GameType
     * @ORM\ManyToOne(targetEntity="ProfideoLeagueBundle\Entity\GameType", inversedBy="seasons", cascade={"persist"})
     * @ORM\JoinColumn(name="game_type_ref", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $gameType;

    //##################################################
    // implemented methods
    //##################################################

    public function is_done(){
        $is_done = true;
        foreach($this->getGames() as $game){
            if(!$game->getIsDone()) {
                $is_done = false;
                return false;
            }
        }
        if(count($this->getGames()) == 0) return false;

        return $is_done;
    }

    public function getGamesByLeague($leagueId)
    {
        $result = [];
        foreach($this->games as $game) {
            if ($game->getLeague()->getId() == $leagueId) {
                array_push($result, $game);
            }
        }
        return $result;
    }

    public function noGamesIsPlayed(){
        $no_games_have_been_played = true;

        foreach ($this->getGames() as $game) {
            if ($game->getIsDone()) {
                $no_games_have_been_played = false;
            }
        }
        return $no_games_have_been_played;
    }

    /**
     * Set games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function setGames($games)
    {
        $this->games = $games;
    }

    //##################################################
    // generated methods
    //##################################################



    /**
     * Constructor
     */
    public function __construct()
    {
        $this->games = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Season
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return Season
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Add game
     *
     * @param \ProfideoLeagueBundle\Entity\Game $game
     *
     * @return Season
     */
    public function addGame(\ProfideoLeagueBundle\Entity\Game $game)
    {
        $this->games[] = $game;

        return $this;
    }

    /**
     * Remove game
     *
     * @param \ProfideoLeagueBundle\Entity\Game $game
     */
    public function removeGame(\ProfideoLeagueBundle\Entity\Game $game)
    {
        $this->games->removeElement($game);
    }

    /**
     * Get games
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGames()
    {
        return $this->games;
    }

    /**
     * Set gameType
     *
     * @param \ProfideoLeagueBundle\Entity\GameType $gameType
     *
     * @return Season
     */
    public function setGameType(\ProfideoLeagueBundle\Entity\GameType $gameType = null)
    {
        $this->gameType = $gameType;

        return $this;
    }

    /**
     * Get gameType
     *
     * @return \ProfideoLeagueBundle\Entity\GameType
     */
    public function getGameType()
    {
        return $this->gameType;
    }
}

<?php

namespace ProfideoLeagueBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
//
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Security\Core\Encoder\PasswordEncoderInterface;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="ProfideoLeagueBundle\Repository\UserRepository")
 * @ORM\Table(name="user")
 */
class User implements UserInterface
{
    //##################################################
    //general
    //##################################################

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="username", type="string", length=255, unique=true, nullable=false, options={"default"= ""})
     */
    private $username;

    /**
     * @ORM\Column(name="lastname", type="string", length=255, nullable=false, options={"default"= ""})
     */
    private $lastname;

    /**
     * @ORM\Column(name="firstname", type="string", length=255, nullable=false, options={"default"= ""})
     */
    private $firstname;

    /**
     * @ORM\Column(name="email", type="string", length=255, nullable=false, options={"default"= ""})
     */
    private $email;

    /**
     * @ORM\Column(name="salt", type="string", length=255)
     */
    private $salt;

    /**
     * @ORM\Column(name="password", type="string", length=255, nullable=false)
     */
    protected $password;
    protected $rawPassword;

    /**
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true, options={"default"= ""})
     */
    private $avatar;

    /**
     * @ORM\Column(name="is_actived", type="boolean", nullable=true, options={"default"= true})
     */
    private $is_actived = 0;

    /**
     * @ORM\Column(name="last_connexion", type="datetime", nullable=true)
     */
    protected $lastConnexion;

    /**
     * @ORM\Column(name="roles", type="string", length=255, nullable=false)
     */
    private $roles = array();

    /**
     * @ORM\OneToMany(targetEntity="ProfideoLeagueBundle\Entity\Game", mappedBy="user_home", cascade={"persist"})
     */
    private $games_home;

    /**
     * @ORM\OneToMany(targetEntity="ProfideoLeagueBundle\Entity\Game", mappedBy="user_visitor", cascade={"persist"})
     */
    private $games_visitor;

    /*
     * @var League
     * @ORM\ManyToOne(targetEntity="ProfideoLeagueBundle\Entity\League", inversedBy="users", cascade={"persist"})
     * @ORM\JoinColumn(name="league_ref", referencedColumnName="id", nullable=true, onDelete="CASCADE")

    private $league;*/

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="League")
     * @ORM\JoinTable(name="user_has_league",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")},
     *      inverseJoinColumns={@ORM\JoinColumn(name="league_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")},
     * )
     */
    private $leagues;

    //##################################################
    // implemented methods
    //##################################################

    public function __toString() {
        //return $this->username;
        return $this->getFullname();
    }

    public function getSalt() {
        return null;
    }

    public function getFullname(){
        return $this->getFirstname() . ' ' . $this->getLastname();
    }

    public function eraseCredentials() {

    }

    public function getRawPassword() {
        return $this->rawPassword;
    }

    public function setRawPassword($rawPassword) {
        $this->rawPassword = $rawPassword;

        return $this;
    }

    public function exist(UserInterface $user)
    {
        if (!$user instanceof User) {
            return false;
        }

        if ($this->username !== $user->getUsername()) {
            return false;
        }

        return true;
    }

    public function equals(User $user)
    {
        return $user->getUsername() == $this->getUsername();
    }

    public function gravatar(){
        if(!empty($this->getAvatar())) return '/users/'.$this->getAvatar();
        $size = 15;
        $default = '';
        return "https://www.gravatar.com/avatar/" . md5(strtolower( trim( $this->email ) )) . "?d=mm" . urlencode( $default ); // . "&s=" . $size;
    }

    public function tostdclass(){
        $result = new \stdClass();
        $result->id = $this->id;
        $result->username = $this->username;
        $result->lastname = $this->lastname;
        $result->firstname = $this->firstname;
        $result->fullname = $this->lastname . ' ' .$this->firstname;
        $result->email = $this->email;
        $result->is_admin = $this->roles === 'USER_ADMIN' ? 1 : 0;
        $result->actived = $this->is_actived ? 1 : 0;
        $result->leagues = [];
        $result->leagues_names = [];
        foreach($this->getLeagues() as $league){
            array_push($result->leagues, $league->getId());
            array_push($result->leagues_names, $league->getName(). ' (' . $league->getGameType()->getLabel() .')');
        }
        //$result->league_id = $this->getLeague() ? $this->getLeague()->getId() : null;
        //$result->league_name = $this->getLeague() ? $this->getLeague()->getName() : '';
        return $result;
    }

    public function getRoles()
    {
        return array($this->roles);
        //return array('ROLE_ADMIN');
    }

    public function isAdmin(){
        return $this->roles === 'ROLE_ADMIN';
    }
    public function isUser(){
        return $this->roles === 'ROLE_USER';
    }

    public function getUsername(){
        return $this->username;
    }
    public function getPassword(){
        return $this->password;
    }

    /**
     * Get leagues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function setLeagues($leagues)
    {
        return $this->leagues = $leagues;
    }

    //##################################################
    // generated methods
    //##################################################


    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->games_home = new \Doctrine\Common\Collections\ArrayCollection();
        $this->games_visitor = new \Doctrine\Common\Collections\ArrayCollection();
        $this->leagues = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set salt
     *
     * @param string $salt
     *
     * @return User
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Set avatar
     *
     * @param string $avatar
     *
     * @return User
     */
    public function setAvatar($avatar)
    {
        $this->avatar = $avatar;

        return $this;
    }

    /**
     * Get avatar
     *
     * @return string
     */
    public function getAvatar()
    {
        return $this->avatar;
    }

    /**
     * Set isActived
     *
     * @param boolean $isActived
     *
     * @return User
     */
    public function setIsActived($isActived)
    {
        $this->is_actived = $isActived;

        return $this;
    }

    /**
     * Get isActived
     *
     * @return boolean
     */
    public function getIsActived()
    {
        return $this->is_actived;
    }

    /**
     * Set lastConnexion
     *
     * @param \DateTime $lastConnexion
     *
     * @return User
     */
    public function setLastConnexion($lastConnexion)
    {
        $this->lastConnexion = $lastConnexion;

        return $this;
    }

    /**
     * Get lastConnexion
     *
     * @return \DateTime
     */
    public function getLastConnexion()
    {
        return $this->lastConnexion;
    }

    /**
     * Set roles
     *
     * @param string $roles
     *
     * @return User
     */
    public function setRoles($roles)
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * Add gamesHome
     *
     * @param \ProfideoLeagueBundle\Entity\Game $gamesHome
     *
     * @return User
     */
    public function addGamesHome(\ProfideoLeagueBundle\Entity\Game $gamesHome)
    {
        $this->games_home[] = $gamesHome;

        return $this;
    }

    /**
     * Remove gamesHome
     *
     * @param \ProfideoLeagueBundle\Entity\Game $gamesHome
     */
    public function removeGamesHome(\ProfideoLeagueBundle\Entity\Game $gamesHome)
    {
        $this->games_home->removeElement($gamesHome);
    }

    /**
     * Get gamesHome
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGamesHome()
    {
        return $this->games_home;
    }

    /**
     * Add gamesVisitor
     *
     * @param \ProfideoLeagueBundle\Entity\Game $gamesVisitor
     *
     * @return User
     */
    public function addGamesVisitor(\ProfideoLeagueBundle\Entity\Game $gamesVisitor)
    {
        $this->games_visitor[] = $gamesVisitor;

        return $this;
    }

    /**
     * Remove gamesVisitor
     *
     * @param \ProfideoLeagueBundle\Entity\Game $gamesVisitor
     */
    public function removeGamesVisitor(\ProfideoLeagueBundle\Entity\Game $gamesVisitor)
    {
        $this->games_visitor->removeElement($gamesVisitor);
    }

    /**
     * Get gamesVisitor
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getGamesVisitor()
    {
        return $this->games_visitor;
    }

    /**
     * Add league
     *
     * @param \ProfideoLeagueBundle\Entity\League $league
     *
     * @return User
     */
    public function addLeague(\ProfideoLeagueBundle\Entity\League $league)
    {
        $this->leagues[] = $league;

        return $this;
    }

    /**
     * Remove league
     *
     * @param \ProfideoLeagueBundle\Entity\League $league
     */
    public function removeLeague(\ProfideoLeagueBundle\Entity\League $league)
    {
        $this->leagues->removeElement($league);
    }

    /**
     * Get leagues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLeagues()
    {
        return $this->leagues;
    }
}

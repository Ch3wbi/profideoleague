<?php

namespace ProfideoLeagueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ProfideoLeagueBundle\Repository\LeagueRepository")
 * @ORM\Table(name="league")
 */
class League
{
    //##################################################
    //general
    //##################################################

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="name", type="string", length=255, nullable=false, options={"default"= ""})
     */
    private $name;

    /**
     * @ORM\Column(name="level", type="integer", nullable=true)
     */
    private $level = 0;

    /**
     * @ORM\Column(name="number_to_upgrade", type="integer", nullable=true)
     */
    private $number_to_upgrade = 0;

    /**
     * @ORM\Column(name="number_to_downgrade", type="integer", nullable=true)
     */
    private $number_to_downgrade = 0;

    /*
     * @ORM\OneToMany(targetEntity="ProfideoLeagueBundle\Entity\User", mappedBy="league", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="id")

    private $users;*/

    /**
     * @var GameType
     * @ORM\ManyToOne(targetEntity="ProfideoLeagueBundle\Entity\GameType", inversedBy="leagues", cascade={"persist"})
     * @ORM\JoinColumn(name="game_type_ref", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $gameType;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="leagues")
     */
    private $users;

    //##################################################
    // implemented methods
    //##################################################

    public function getNbDays(){
        //return count($this->getUsers()) - 1;
        return count($this->getUsers()) - 1;
    }

    public function getNbTotalGame(){
        //return $this->getNbDays() * (count($this->getUsers()) / 2);
        return (count($this->getUsers()) / 2) * $this->getNbDays();
    }

    public function getNbGameByDay(){
        return floor(count($this->getUsers()) / 2);
    }

    public function getNbMaxHome(){
        return floor($this->getNbDays() / 2);
    }

    public function tostdclass(){
        $result = new \stdClass();
        $result->id = $this->id;
        $result->name = $this->name;
        $result->level = $this->level;
        $result->number_to_upgrade = $this->number_to_upgrade;
        $result->number_to_downgrade = $this->number_to_downgrade;
        $result->gametype = $this->getGameType()->getId();
        $result->gametype_label = $this->getGameType()->getlabel();
        return $result;
    }

    //##################################################
    // generated methods
    //##################################################


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return League
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return League
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set numberToUpgrade
     *
     * @param integer $numberToUpgrade
     *
     * @return League
     */
    public function setNumberToUpgrade($numberToUpgrade)
    {
        $this->number_to_upgrade = $numberToUpgrade;

        return $this;
    }

    /**
     * Get numberToUpgrade
     *
     * @return integer
     */
    public function getNumberToUpgrade()
    {
        return $this->number_to_upgrade;
    }

    /**
     * Set numberToDowngrade
     *
     * @param integer $numberToDowngrade
     *
     * @return League
     */
    public function setNumberToDowngrade($numberToDowngrade)
    {
        $this->number_to_downgrade = $numberToDowngrade;

        return $this;
    }

    /**
     * Get numberToDowngrade
     *
     * @return integer
     */
    public function getNumberToDowngrade()
    {
        return $this->number_to_downgrade;
    }

    /**
     * Add user
     *
     * @param \ProfideoLeagueBundle\Entity\User $user
     *
     * @return League
     */
    public function addUser(\ProfideoLeagueBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \ProfideoLeagueBundle\Entity\User $user
     */
    public function removeUser(\ProfideoLeagueBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set gameType
     *
     * @param \ProfideoLeagueBundle\Entity\GameType $gameType
     *
     * @return League
     */
    public function setGameType(\ProfideoLeagueBundle\Entity\GameType $gameType = null)
    {
        $this->gameType = $gameType;

        return $this;
    }

    /**
     * Get gameType
     *
     * @return \ProfideoLeagueBundle\Entity\GameType
     */
    public function getGameType()
    {
        return $this->gameType;
    }
}

<?php

namespace ProfideoLeagueBundle\Entity;

use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ProfideoLeagueBundle\Repository\GameTypeRepository")
 * @ORM\Table(name="game_type")
 */
class GameType
{
    //##################################################
    //general
    //##################################################

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="label", type="string", length=255, nullable=false, options={"default"= ""})
     */
    private $label;

    /**
     * @ORM\OneToMany(targetEntity="ProfideoLeagueBundle\Entity\Season", mappedBy="gameType", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    private $seasons;

    /**
     * @ORM\OneToMany(targetEntity="ProfideoLeagueBundle\Entity\League", mappedBy="gameType", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="id")
     */
    private $leagues;

    //##################################################
    // implemented methods
    //##################################################

    public function getCurrentSeason(){
        $seasons = $this->getSeasons();

        $sort = Criteria::create();
        $sort->orderBy(Array(
            'number' => Criteria::DESC
        ));

        $season = $seasons->matching($sort);

        return $season->first();
    }

    //##################################################
    // generated methods
    //##################################################


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->seasons = new \Doctrine\Common\Collections\ArrayCollection();
        $this->leagues = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set label
     *
     * @param string $label
     *
     * @return GameType
     */
    public function setLabel($label)
    {
        $this->label = $label;

        return $this;
    }

    /**
     * Get label
     *
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * Add season
     *
     * @param \ProfideoLeagueBundle\Entity\Season $season
     *
     * @return GameType
     */
    public function addSeason(\ProfideoLeagueBundle\Entity\Season $season)
    {
        $this->seasons[] = $season;

        return $this;
    }

    /**
     * Remove season
     *
     * @param \ProfideoLeagueBundle\Entity\Season $season
     */
    public function removeSeason(\ProfideoLeagueBundle\Entity\Season $season)
    {
        $this->seasons->removeElement($season);
    }

    /**
     * Get seasons
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSeasons()
    {
        return $this->seasons;
    }

    /**
     * Add league
     *
     * @param \ProfideoLeagueBundle\Entity\League $league
     *
     * @return GameType
     */
    public function addLeague(\ProfideoLeagueBundle\Entity\League $league)
    {
        $this->leagues[] = $league;

        return $this;
    }

    /**
     * Remove league
     *
     * @param \ProfideoLeagueBundle\Entity\League $league
     */
    public function removeLeague(\ProfideoLeagueBundle\Entity\League $league)
    {
        $this->leagues->removeElement($league);
    }

    /**
     * Get leagues
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLeagues()
    {
        return $this->leagues;
    }
}

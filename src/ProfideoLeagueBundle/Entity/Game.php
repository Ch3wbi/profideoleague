<?php

namespace ProfideoLeagueBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="ProfideoLeagueBundle\Repository\GameRepository")
 * @ORM\Table(name="game")
 */
class Game
{
    //##################################################
    //general
    //##################################################

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\Column(name="score_home", type="integer", nullable=true)
     */
    private $score_home = 0;

    /**
     * @ORM\Column(name="score_visitor", type="integer", nullable=true)
     */
    private $score_visitor = 0;

    /**
     * @ORM\Column(name="is_done", type="boolean", nullable=true, options={"default"= false})
     */
    private $is_done = 0;

    /**
     * @var League
     * @ORM\ManyToOne(targetEntity="ProfideoLeagueBundle\Entity\League", cascade={"persist"})
     * @ORM\JoinColumn(name="league_ref", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $league;

    /**
     * @var Season
     * @ORM\ManyToOne(targetEntity="ProfideoLeagueBundle\Entity\Season", inversedBy="games", cascade={"persist"})
     * @ORM\JoinColumn(name="season_ref", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $season;

    /**
     * @var UserHome
     * @ORM\ManyToOne(targetEntity="ProfideoLeagueBundle\Entity\User", inversedBy="games_home", cascade={"persist"})
     * @ORM\JoinColumn(name="user_home_ref", referencedColumnName="id", nullable=true, onDelete="CASCADE")
    */
    private $user_home;

    /**
     * @var UserVisitor
     * @ORM\ManyToOne(targetEntity="ProfideoLeagueBundle\Entity\User", inversedBy="games_visitor", cascade={"persist"})
     * @ORM\JoinColumn(name="user_visitor_ref", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     */
    private $user_visitor;


    //##################################################
    // implemented methods
    //##################################################


    //##################################################
    // generated methods
    //##################################################



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set scoreHome
     *
     * @param integer $scoreHome
     *
     * @return Game
     */
    public function setScoreHome($scoreHome)
    {
        $this->score_home = $scoreHome;

        return $this;
    }

    /**
     * Get scoreHome
     *
     * @return integer
     */
    public function getScoreHome()
    {
        return $this->score_home;
    }

    /**
     * Set scoreVisitor
     *
     * @param integer $scoreVisitor
     *
     * @return Game
     */
    public function setScoreVisitor($scoreVisitor)
    {
        $this->score_visitor = $scoreVisitor;

        return $this;
    }

    /**
     * Get scoreVisitor
     *
     * @return integer
     */
    public function getScoreVisitor()
    {
        return $this->score_visitor;
    }

    /**
     * Set isDone
     *
     * @param boolean $isDone
     *
     * @return Game
     */
    public function setIsDone($isDone)
    {
        $this->is_done = $isDone;

        return $this;
    }

    /**
     * Get isDone
     *
     * @return boolean
     */
    public function getIsDone()
    {
        return $this->is_done;
    }

    /**
     * Set league
     *
     * @param League $league
     *
     * @return Game
     */
    public function setLeague(League $league = null)
    {
        $this->league = $league;

        return $this;
    }

    /**
     * Get league
     *
     * @return League
     */
    public function getLeague()
    {
        return $this->league;
    }

    /**
     * Set season
     *
     * @param \ProfideoLeagueBundle\Entity\Season $season
     *
     * @return Game
     */
    public function setSeason(\ProfideoLeagueBundle\Entity\Season $season = null)
    {
        $this->season = $season;

        return $this;
    }

    /**
     * Get season
     *
     * @return \ProfideoLeagueBundle\Entity\Season
     */
    public function getSeason()
    {
        return $this->season;
    }

    /**
     * Set userHome
     *
     * @param \ProfideoLeagueBundle\Entity\User $userHome
     *
     * @return Game
     */
    public function setUserHome(\ProfideoLeagueBundle\Entity\User $userHome = null)
    {
        $this->user_home = $userHome;

        return $this;
    }

    /**
     * Get userHome
     *
     * @return \ProfideoLeagueBundle\Entity\User
     */
    public function getUserHome()
    {
        return $this->user_home;
    }

    /**
     * Set userVisitor
     *
     * @param \ProfideoLeagueBundle\Entity\User $userVisitor
     *
     * @return Game
     */
    public function setUserVisitor(\ProfideoLeagueBundle\Entity\User $userVisitor = null)
    {
        $this->user_visitor = $userVisitor;

        return $this;
    }

    /**
     * Get userVisitor
     *
     * @return \ProfideoLeagueBundle\Entity\User
     */
    public function getUserVisitor()
    {
        return $this->user_visitor;
    }
}

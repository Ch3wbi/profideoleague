<?php

namespace ProfideoLeagueBundle\Authentification\Handler;

/**
 * Created by PhpStorm.
 * User: ch3wbi
 * Date: 03/04/17
 * Time: 15:56
 */
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Http\Authentication\AuthenticationSuccessHandlerInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Security;
use ProfideoLeagueBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;


/**
 * Description of LoginSuccessHandler
 *
 * @author Olivier
 */
class LoginSuccessHandler implements AuthenticationSuccessHandlerInterface {

    protected $router;
    protected $tokenStorage;
    protected $em;

    public function __construct(EntityManager $entityManager, Router $router, TokenStorage $tokenStorage)
    {
        $this->router = $router;
        //$this->container = $container;
        $this->em = $entityManager;
        $this->tokenStorage = $tokenStorage;
    }

    public function onAuthenticationSuccess(Request $request, TokenInterface $token)
    {
        // echo $this->security->getToken()->getUser()->getTypeRoleDefault()->getLibelle();die;
        $user = $this->tokenStorage->getToken()->getUser(); //$this->get('security.token_storage')->getToken()->getUser();

        $firstConnexion = false;
        if(!$user->getLastConnexion()) $firstConnexion = true;
        $user->setLastConnexion(new \DateTime());
        $this->em->persist($user);
        $this->em->flush();

        if($firstConnexion){
            return new RedirectResponse($this->router->generate('profideo_league_profil'));
        }else{
            return new RedirectResponse($this->router->generate('profideo_league_homepage'));
        }


    }
}
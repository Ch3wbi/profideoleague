<?php

namespace ProfideoLeagueBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use ProfideoLeagueBundle\Entity\User;

class UserCommand extends ContainerAwareCommand
{
    //ex: php app/console ProfideoLeagueBundle:user:ajouter admin admin 1
    protected function configure()
    {
        $this
            ->setName('ProfideoLeagueBundle:user:ajouter')
            ->setDescription('Ajout ProfideoLeagueBundle utilisateur')
            ->addArgument('username', InputArgument::REQUIRED, 'Identifiant :')
            ->addArgument('password', InputArgument::REQUIRED, 'Mot de passe :')
            ->addArgument('userRole', InputArgument::REQUIRED, 'UserRole (0 => ROLE_USER; 1 => ROLE_ADMIN) :')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $username = $input->getArgument('username');
        $password = $input->getArgument('password');
        $userRole = $input->getArgument('userRole');

        $em = $this->getContainer()->get('doctrine')->getManager();

        $utilisateur = new User();
        $utilisateur->setUsername($username);
        $utilisateur->setLastname($username);
        $utilisateur->setFirstname($username);
        $role = $userRole === 0 ? 'ROLE_USER' : 'ROLE_ADMIN';
        $utilisateur->setRoles($role);

        $utilisateur->setIsActived(true);

        $encoder = $this->getContainer()->get('security.password_encoder'); // encode the password
        $password = $encoder->encodePassword($utilisateur, $password);
        $utilisateur->setPassword($password);

        $em->persist($utilisateur);
        $em->flush();

        $output->writeln(sprintf('Ajout de l\'utilisateur %s de rôle: %s', $username, $role));
    }
}
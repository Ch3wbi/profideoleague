/**
 * Created by ch3wbi on 19/03/2017.
 */

function actionFormatter(value, row, index) {
    var season_isdone = $('#table_leagues').data('seasonisdone');
    var season_games = $('#table_leagues').data('seasongames');
    var result = [];
    result.push('<a class="edit btn btn-default" href="javascript:void(0)" data-tooltip="true" title="Edit">' +
        '<i class="fa fa-pencil-square-o"></i>' +
        '</a>');

    /*if(season_isdone == 1 || season_games == 0) result.push('<a class="remove btn btn-warning" href="javascript:void(0)" title="Remove">' +
        '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
        '</a>');*/
    result.push('<a class="remove btn btn-warning" href="javascript:void(0)" title="Remove">' +
        '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
        '</a>');

    return result.join('');
    /*return [
     '<a class="like" href="javascript:void(0)" title="Like">',
     '<i class="glyphicon glyphicon-heart"></i>',
     '</a>',
     '<a class="edit ml10" href="javascript:void(0)" data-tooltip="true" title="Edit">',
     '<i class="glyphicon glyphicon-edit"></i>',
     '</a>',
     '<a class="remove ml10" href="javascript:void(0)" title="Remove">',
     '<i class="glyphicon glyphicon-remove"></i>',
     '</a>'
     ].join('');*/
}

window.actionEvents = {
    'click .edit': function (e, value, row, index) {
        //alert('You click edit icon, row: ' + JSON.stringify(row));
        edit(row);
    },
    'click .remove': function (e, value, row, index) {
        remove(row);
    }
};
function edit(league){
    $('#league_form').data('league', league.id);
    $('#leagueId').html(league.id);
    $('#name').val(league.name);
    $('#level').val(league.level);
    $('#number_to_upgrade').val(league.number_to_upgrade);
    $('#number_to_downgrade').val(league.number_to_downgrade);
    if(league.gametype) $('#selectGameType').selectpicker('val', league.gametype);
    $("#league_modal").modal();
}
function remove(league){
    bootbox.confirm(
        {
            title: "Suppression",
            message: "En êtes-vous sûr?",
            callback: function(result) {
                if(result){

                    $.ajax({
                        type: 'POST',
                        url: Routing.generate('profideo_league_league_remove'),
                        data: {
                            'id': league.id
                        },
                        success: function (data) {
                            if(data.result){
                                $('#table_leagues').bootstrapTable('removeByUniqueId', league.id);
                            }
                        }
                    });

                }
            }
        });
}
(function($){

    "use strict";

    var g_leagues = [];
    $(document).ready(function () {

        loadLeagues();

        $('#table_leagues').on('refresh.bs.table', function(e, text){
            loadLeagues();
        });

        $('#insertRow').click(function(){
            edit({id: 0, name: '', level: g_leagues.length + 1, number_to_upgrade: 0, number_to_downgrade: 0});
        });

        $('#btnFormLeague').click(function(){
            var league_id = $('#league_form').data('league');
            var name = $('#name').val();
            var level = $('#level').val();
            var number_to_upgrade = $('#number_to_upgrade').val();
            var number_to_downgrade = $('#number_to_downgrade').val();
            var gametype_id = $('#selectGameType').selectpicker('val');

            $.ajax({
                type: 'POST',
                url: Routing.generate('profideo_league_league_edit'),
                data: {
                    'id': league_id,
                    'name': name,
                    'level': level,
                    'number_to_upgrade': number_to_upgrade,
                    'number_to_downgrade': number_to_downgrade,
                    'gametype_id': gametype_id
                },
                success: function (data) {
                    if(data.result){
                        var index = -1;
                        var result = jQuery.grep( g_leagues, function ( element, i )
                        {
                            if(element.id == data.result.id) {
                                index = i;
                                return element;
                            }
                        });
                        if(index > -1){
                            $('#table_leagues').bootstrapTable('updateRow', {
                                index: index,
                                row: data.result
                            });
                        }else{
                            $('#table_leagues').bootstrapTable('insertRow', {
                                index: 1,
                                row: data.result
                            });
                            g_leagues.push(data.result);
                        }
                        $("#league_modal").modal('hide');
                    }
                }
            });
        });

    });





    function loadLeagues(){
        $.ajax({
            type: 'POST',
            url: Routing.generate('profideo_league_leagues'),
            data: {

            },
            success: function (data) {
                g_leagues = data.leagues;

                $('#table_leagues').bootstrapTable({
                    data: g_leagues
                });
            }
        });
    }


})(jQuery);
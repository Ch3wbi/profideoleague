(function($){

	$(document).ready(function () {


//var socket = io.connect('http://ch3wbichat.ddns.net:1337');
		//var socket = io.connect('http://localhost:1337');
		//var socket = io.connect('ch3wbichat.dev:1337');
	try {
		var socket = io.connect('profideoleague.ddns.net:1337');
	}catch(err){
		console.log(err);
	}

	if(socket) {

		connectionToChat();

		$('#showChat').click(function(){
			$('.chat').show('slow');
			$('#showChat').hide();
		});

		$('#hideChat').click(function(){
			$('.chat').hide('slow');
			$('#showChat').show();
		});

		/*window.onbeforeunload = myUnloadEvent;
		 function myUnloadEvent() {
		 console.log("Do your actions in here")
		 */

		var msgtpl = $('#msgtpl').html();
		var usertpl = $('#usertpl').html();

		var users = [];

		var lastmsg = false;
		var title = 'Ch3wbiTchat';

		$('#msgtpl').remove();
		$('#usertpl').remove();

		/*$('#loginform').submit(function(event){
		 event.preventDefault();
		 var username = $('#username').val();
		 var mail = $('#mail').val();
		 if(username.length == 0 && mail.length == 0) return false;
		 socket.emit('login', {
		 username: username,
		 mail: mail
		 });
		 });*/

		function connectionToChat() {
			var avatar = $('#profil_image').data('avatar');
			var username = $('#profil_image').data('username');
			var email = $('#profil_image').data('email');
			socket.emit('login', {
				avatar: avatar,
				username: username,
				mail: email
			});
		}

		function logoutToChat() {
			var avatar = $('#profil_image').data('avatar');
			var username = $('#profil_image').data('username');
			var email = $('#profil_image').data('email');
			socket.emit('logout', {
				username: username
			});
		}

		socket.on('logged', function (user) {
			$('#login').fadeOut();
			//$('.chat').show('slow');
		});

		var windowFocus = false;
		var myInterval;
		$(window).focus(function () {
			windowFocus = true;
			clearInterval(myInterval);
		}).blur(function () {
			windowFocus = false;
			//console.log('out');
		});

		/**
		 * Envois de message
		 **/
		$('#formChat').submit(function (event) {
			event.preventDefault(); //arrete l'event
			socket.emit('newmsg', {message: $('#message').val()});

			$('#message').val('');
			$('#message').focus();

			//window.location.replace(Routing.generate('ch3wbi_chat_index'));


		});

		//récupération de l'event notification
		socket.on('notification', function () {
//		var timer = setInterval
			if (!windowFocus) {
				console.log('ntoo');
				if (!("Notification" in window)) {
					console.log('pas');
					myInterval = setInterval(function () {
						document.title = "Nouveau message!";
						setTimeout(function () {
							document.title = title;
						}, 3000);
					}, 1000);
				} else {
					console.log("permission:" + Notification.permission);
					if (Notification.permission !== 'denied') {
						Notification.requestPermission(function (permission) {

							// Quelque soit la réponse de l'utilisateur, nous nous assurons de stocker cette information
							if (!('permission' in Notification)) {
								Notification.permission = permission;
							}

						});
					}
					// Si l'utilisateur est OK, on crée une notification
					if (Notification.permission === "granted") {
						var notification = new Notification("Nouveau message sur ch3wbichat !");
					}
				}

			}
		});
		socket.on('newmsg', function (m) {
			console.log(m.message);
			$message = $('<div class="chat-message"></div>');
			$message.append('<div title="' + m.h + ':' + m.m + '" class="user" style="background-image: url(' + m.user.avatar + ')"></div>');
			$message.append('<div class="message">' + m.message + '</div>')
			$('.chat-messages').append($message);
			/*$message.append('<img src="' + message.user.avatar + '">');
			 $infos = $('<div class="info"></div>');
			 $infos.append('<p><strong>' + message.user.username + '</strong></p>');
			 $infos.append('<p>' + message.message + '</p>');
			 $infos.append('<span class="date">' + message.h + ':' + message.m + '</span>');*/
			//$message.append($infos);


			if (lastmsg != m.user.id) {
				//$('.chat-messages').append('<div class="separator"></div>');
				$('.chat-messages').addClass('message-seperator');
				lastmsg = m.user.id;
			}
			$('.chat-messages').append($message);
			$('.chat-messages').animate({scrollTop: $('.chat-messages').prop('scrollHeight')}, 50);


		});

		/**
		 * Gestion des connectés
		 **/
		socket.on('newusr', function (user) {
			/*console.log(user);

			 if(!isInArray(users, 'username', user.username, false)){

			 }

			 $circles = $('<div class="animated-circles js-animated-circles animated"><div class="circle c-1"></div><div class="circle c-2">' +
			 '</div><div class="circle c-3"></div></div>');
			 $user = $('<div class="user"></div>');
			 $user.attr('id', 'user_' + user.id);
			 $image = $('<div class="user-profil-image"></div>');
			 $image.append($circles);
			 $avatar = $('<div class="user-profil-image-avatar"></div>');
			 $avatar.css('background-image', 'url(' + user.avatar + ')');
			 $image.append($avatar);
			 $user.append($image);
			 $user.append('<div class="user-profil-username">' + user.username + '</div>');

			 $('.chat-users').find('.chat-users-list').append($user);
			 //$('.chat-users').find('.chat-users-list').append(Mustache.render(usertpl, user));
			 $('.chat-users-title').html('Chat (' + $('.chat-users').find('.user').length + ')');


			 users.push(user);

			 var tmp = searchInArray(users, 'username', 'moulin', false);
			 */
			//$('.chat-users').append('<img src="' + user.avatar + '" id="' + user.id + '"> ' + user.username);
			//$('#users').append('<img src="' + user.avatar + '" id="' + user.id + '">');
		});

		socket.on('disusr', function (user) {


			$('#user_' + user.id).remove();
			$('.chat-users-title').html('Chat (' + $('.chat-users').find('.user').length + ')');
		});

	}else{
		$('#showChat').hide();
	}

	});

})(jQuery);

/**
 * Created by ch3wbi on 28/03/2017.
 */

(function($){

    "use strict";

    $(document).ready(function () {

        $('#removeAvatar').click(function(){
            $.ajax({
                type: 'POST',
                url: Routing.generate('profideo_league_remove_avatar'),
                data: {
                    'id': $(this).data('user')
                },
                success: function (data) {
                    location.reload();
                }
            });
        });

    });





    function loadLeagues(){
        $.ajax({
            type: 'POST',
            url: Routing.generate('profideo_league_leagues'),
            data: {

            },
            success: function (data) {

                $('#table_leagues').bootstrapTable({
                    data: g_leagues
                });
            }
        });
    }


})(jQuery);
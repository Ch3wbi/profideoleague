/**
 * Created by ch3wbi on 11/03/2017.
 */

function actionFormatter(value, row, index) {
    /*var season_isdone = $('#table_users').data('seasonisdone');
    var season_games = $('#table_users').data('seasongames');*/
    var result = [];
    result.push('<a class="edit btn btn-default" href="javascript:void(0)" data-tooltip="true" title="Edit">' +
        '<i class="fa fa-pencil-square-o"></i>' +
        '</a>');

    /*if(season_isdone == 1 || season_games == 0) result.push('<a class="remove btn btn-danger" href="javascript:void(0)" title="Remove">' +
        '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
        '</a>');*/
    if(row.actived){
        result.push('<a class="remove btn btn-warning" href="javascript:void(0)" title="Désactiver">' +
            '<i class="fa fa-calendar-times-o" aria-hidden="true"></i>' +
            '</a>');
    }else{
        result.push('<a class="remove btn btn-success" href="javascript:void(0)" title="Activer">' +
            '<i class="fa fa-calendar-check-o" aria-hidden="true"></i>' +
            '</a>');
    }


    return result.join('');
    /*return [
        '<a class="like" href="javascript:void(0)" title="Like">',
        '<i class="glyphicon glyphicon-heart"></i>',
        '</a>',
        '<a class="edit ml10" href="javascript:void(0)" data-tooltip="true" title="Edit">',
        '<i class="glyphicon glyphicon-edit"></i>',
        '</a>',
        '<a class="remove ml10" href="javascript:void(0)" title="Remove">',
        '<i class="glyphicon glyphicon-remove"></i>',
        '</a>'
    ].join('');*/
}
function formatAdmin(value, row, index){
    return row.is_admin == 1 ? 'Oui' : 'Non';
}
function formatActived(value, row, index){
    return row.actived == 1 ? 'Oui' : 'Non';
}
window.actionEvents = {
    'click .like': function (e, value, row, index) {
        alert('You click like icon, row: ' + JSON.stringify(row));
        console.log(value, row, index);
    },
    'click .edit': function (e, value, row, index) {
        //alert('You click edit icon, row: ' + JSON.stringify(row));
        edit(row);
    },
    'click .remove': function (e, value, row, index) {
        remove(row);
    }
};
function edit(user){

    $('#user_form').data('user', user.id);
    $('#userId').html(user.id);
    $('#username').val(user.username);
    $('#lastname').val(user.lastname);
    $('#firstname').val(user.firstname);
    $('#email').val(user.email);
    $('#selectLeague').val(user.leagues);
    //$('#selectLeague').change();
    $('#selectLeague').selectpicker('val', user.leagues);
    //$('#selectLeague').on('refreshed.bs.select');
    //$('#admin').val(user.is_admin);
    $('input:radio[name=admin][value=' + (user.is_admin == 1 ? 1 : 0)+ ']').prop('checked', true);
    $("#userForm").modal();
}
function remove(user){
    bootbox.confirm(
        {
            title: "Suppression",
            message: "En êtes-vous sûr?",
            callback: function(result) {
                if(result){

                    $.ajax({
                        type: 'POST',
                        url: Routing.generate('profideo_league_user_remove'),
                        data: {
                            'id': user.id
                        },
                        success: function (data) {
                            if(data.result){
                                $('#table_users').bootstrapTable('updateByUniqueId', {id: user.id, row: data.result });
                                //$('#table_users').bootstrapTable('removeByUniqueId', user.id);
                            }
                        }
                    });

                }
            }
        });

}
(function($){

    "use strict";

    var g_users = [];
    $(document).ready(function () {
        //console.log(users);

        loadUsers();

        $('#sendMailsWelcome').click(function(){
            $.ajax({
                type: 'POST',
                url: Routing.generate('profideo_league_send_mails_welcome'),
                data: {
                },
                success: function (data) {

                }
            });
        });

        $('#table_users').on('refresh.bs.table', function(e, text){
            loadUsers();
        });

        $('#insertRow').click(function(){
            edit({id: 0, username: '', lastname: '', firstname: '', email: '', league_id: -1, is_admin: 0});
        });

        $('#btnFormUser').click(function(){
            var user_id = $('#user_form').data('user');
            var username = $('#username').val();
            var lastname = $('#lastname').val();
            var firstname = $('#firstname').val();
            var email = $('#email').val();
            var leagues = $('#selectLeague').selectpicker('val');

            var admin = $('input[type=radio][name=admin]:checked').attr('value');

            $.ajax({
                type: 'POST',
                url: Routing.generate('profideo_league_user_edit'),
                data: {
                    'id': user_id,
                    'username': username,
                    'lastname': lastname,
                    'firstname': firstname,
                    'email': email,
                    'leagues': leagues,
                    'admin': admin
                },
                success: function (data) {
                    if(data.result){
                        var index = -1;
                        var result = jQuery.grep( g_users, function ( element, i )
                        {
                            if(element.id == data.result.id) {
                                index = i;
                                return element;
                            }
                        });
                        if(index > -1){
                            $('#table_users').bootstrapTable('updateRow', {
                                index: index,
                                row: data.result
                            });
                        }else if(user_id == 0){
                            $('#table_users').bootstrapTable('insertRow', {
                                index: 1,
                                row: data.result
                            });
                            g_users.push(data.result);
                        }
                        $("#userForm").modal('hide');
                    }
                }
            });
        });

    });

    function loadUsers(){
        $.ajax({
            type: 'POST',
            url: Routing.generate('profideo_league_users_list'),
            data: {

            },
            success: function (data) {
                g_users = data.users;

                $('#table_users').bootstrapTable({
                    data: g_users
                });
                return false;
                /*$('#table_users tbody').find('tr').remove();
                $.each(data.users, function( index, user ) {
                    console.log( user.login );

                        var $tr = $('<tr></tr>');
                        $tr.attr('id', user.id);
                        var $td = $('<td>' + user.id + '</td>');
                        $tr.append($td);
                        $td = $('<td>' + user.login + '</td>');
                        $tr.append($td);
                        $td = $('<td>' + user.lastname + ' ' + user.firstname + '</td>');
                        $tr.append($td);
                        $td = $('<td>' + user.email + '</td>');
                        $tr.append($td);
                        $td = $('<td>' + user.league_name + '</td>');
                        $tr.append($td);
                        $td = $('<td>' + (user.is_admin == 1 ? 'Oui' : 'Non') + '</td>');
                        $tr.append($td);
                        var $buttonEdit = $('<button type="button" data-user=" ' + user.id + '" class="user_edit btn btn-default"></button>');
                        $buttonEdit.append('<i class="fa fa-pencil" aria-hidden="true"></i>');
                        var $buttonDelete = $('<button type="button" data-user=" ' + user.id + '" class="user_edit btn btn-danger"></button>');
                        $buttonDelete.append('<i class="fa fa-remove" aria-hidden="true"></i>');
                        $tr.append($buttonEdit);
                        $tr.append($buttonDelete);
                        $('#table_users tbody').append($tr);

                });*/



            }
        });
    }


})(jQuery);
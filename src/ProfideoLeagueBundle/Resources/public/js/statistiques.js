/**
 * Created by ch3wbi on 11/03/2017.
 */
(function($){

    "use strict";

    $(document).ready(function () {

        $('.selectSeason').change(function(){
            showStats($(this).data('gametype'), $(this).data('league'));
            getClassement($(this).data('gametype'), $(this).val(), $(this).data('league'));
        });
        /*$('#selectLeague').change(function(){
            showStats();
        });*/



        $('.nav-tabs a').on('shown.bs.tab', function(event){
            var x = $(event.target).text();         // active tab
            var y = $(event.relatedTarget).text();  // previous tab
            //console.log(x + ' '+ y);
            var gametype = $(this).data('gametype');
            var league = $(this).data('league');
            var season_id = $('#selectSeason_' + gametype + '_' + league).val();

            showStats(gametype, league);
            getClassement($(this).data('gametype'), season_id, $(this).data('league'));
        });


        /*new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'myfirstchart',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [
                { year: '2008', value: 20 },
                { year: '2009', value: 10 },
                { year: '2010', value: 5 },
                { year: '2011', value: 5 },
                { year: '2012', value: 20 }
            ],
            // The name of the data record attribute that contains x-values.
            xkey: 'year',
            // A list of names of data record attributes that contain y-values.
            ykeys: ['value'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['Value']
        });*/



    });

    function showStats(gametype_id, league_id){
        var season_id = $('#selectSeason_' + gametype_id + '_' + league_id).val();
        //var league_id = $('#selectLeague').val();

        $('#chats_buts_' + gametype_id + '_' + league_id).empty();
        $('#chats_buts2_' + gametype_id + '_' + league_id).empty();
        $('#bar-games_' + gametype_id + '_' + league_id).empty();
        $.ajax({
            type: 'POST',
            url: Routing.generate('profideo_league_statistiques_buts'),
            data: {
                "season_id": season_id,
                "league_id": league_id
            },
            success: function (data) {
                var bp = [];
                var bc = [];
                var games = [];

                var g = { y: 'Matchs gagnés'};
                var n = { y: 'Matchs nuls'};
                var p = { y: 'Matchs perdus'};
                games.push(g);
                games.push(n);
                games.push(p);

                var users = [];


                $.each(data.users, function( index, user ) {
                    bp.push({'label': user.user_fullname, value: user.bp });
                    bc.push({'label': user.user_fullname, value: user.bc });
                    g[user.user_fullname] = user.g;
                    n[user.user_fullname] = user.n;
                    p[user.user_fullname] = user.p;

                    users.push(user.user_fullname);
                });

                Morris.Donut({
                    element: 'chats_buts_' + gametype_id + '_' + league_id,
                    data: bp
                });

                Morris.Donut({
                    element: 'chats_buts2_' + gametype_id + '_' + league_id,
                    data: bc
                });

                Morris.Bar({
                    element: 'bar-games_' + gametype_id + '_' + league_id,
                    data: games,
                    xkey: 'y',
                    ykeys: users,
                    labels: users

                });

            }
        });
    }


    function getClassement(gametype_id, seasonId, league_id){
console.log('toto');
        $.ajax({
            type: 'POST',
            url: Routing.generate('profideo_league_refresh_classement'),
            data: {
                "season": seasonId,
                "league": league_id
            },
            success: function (data) {
                //classement_{{ gameType.getCurrentSeason().getId() }}_{{ league.getId() }}
                $('#classement_' + gametype_id + '_' + league_id).empty();
                $('#classement_' + gametype_id + '_' + league_id).html(data);
            }
        });
    }

})(jQuery);
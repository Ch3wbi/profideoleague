/**
 * Created by ch3wbi on 18/03/2017.
 */

function actionFormatter(value, row, index) {
    var result = [];
    result.push('<a class="edit btn btn-default" href="javascript:void(0)" data-tooltip="true" title="Edit">' +
        '<i class="fa fa-pencil-square-o"></i>' +
        '</a>');

    result.push('<a class="remove btn btn-warning" href="javascript:void(0)" title="Remove">' +
        '<i class="fa fa-trash-o" aria-hidden="true"></i>' +
        '</a>');

    return result.join('');
}

window.actionEvents = {
    'click .like': function (e, value, row, index) {
        alert('You click like icon, row: ' + JSON.stringify(row));
        console.log(value, row, index);
    },
    'click .edit': function (e, value, row, index) {
        //alert('You click edit icon, row: ' + JSON.stringify(row));
        edit(row);
    },
    'click .remove': function (e, value, row, index) {
        remove(row);
    }
};
function edit(gametype){
    $('#gametype_form').data('gametype', gametype.id);
    $('#gametypeId').html(gametype.id);
    $('#label').val(gametype.label);
    $("#gametypeForm").modal();
}
function remove(gametype){
    $.ajax({
        type: 'POST',
        url: Routing.generate('profideo_league_gametype_remove'),
        data: {
            'id': gametype.id
        },
        success: function (data) {
            if(data.result){
                $('#table_gametypes').bootstrapTable('removeByUniqueId', gametype.id);
            }
        }
    });
}
(function($){

    "use strict";

    var g_gametypes = [];
    $(document).ready(function () {

        loadGameTypes();

        $('#table_gametypes').on('refresh.bs.table', function(e, text){
            loadGameTypes();
        });

        $('#insertRow').click(function(){
            edit({id: 0, label: ''});

            /*$('#table_gametypes').bootstrapTable('insertRow', {
                index: 1,
                row: {
                    id: 0,
                    label: ''
                }
            })*/
        });

        $('#btnFormGameType').click(function(){
            var gametype_id = $('#gametype_form').data('gametype');
            var label = $('#label').val();

            $.ajax({
                type: 'POST',
                url: Routing.generate('profideo_league_gametype_edit'),
                data: {
                    'id': gametype_id,
                    'label': label,
                },
                success: function (data) {
                    if(data.result){
                        var index = -1;
                        var result = jQuery.grep( g_gametypes, function ( element, i )
                        {
                            if(element.id == data.result.id) {
                                index = i;
                                return element;
                            }
                        });
                        if(index > -1){
                            $('#table_gametypes').bootstrapTable('updateRow', {
                                index: index,
                                row: data.result
                            });
                        }else{
                            g_gametypes.push(data.result);
                            $('#table_gametypes').bootstrapTable('insertRow', {
                                index: 1,
                                row: data.result
                            });
                        }
                        $("#gametypeForm").modal('hide');
                    }
                }
            });
        });

    });





    function loadGameTypes(){
        $.ajax({
            type: 'POST',
            url: Routing.generate('profideo_league_gametypes'),
            data: {

            },
            success: function (data) {

                g_gametypes = data.gametypes;

                $('#table_gametypes').bootstrapTable({
                    data: g_gametypes
                });
            }
        });
    }


})(jQuery);
/**
 * Created by ch3wbi on 26/01/17.
 */

function searchInArray(data, key, search, forAllCharacters){
    var result = jQuery.grep( data, function ( element, index )
    {
        if(!jQuery.isNumeric(search)){
            if(search == 'undefined' || !search) search = '';
        }

        //if(!jQuery.isNumeric(search)) search = '';
        if( element[key] || jQuery.isNumeric(element[key])){
            if(forAllCharacters){
                if ( element[key].toString().toLowerCase() === search.toString().toLowerCase() )
                {
                    return element;
                }
            }else{
                if ( element[key].toString().toLowerCase().indexOf( search.toString().toLowerCase() ) != -1)
                {
                    return element;
                }
            }
        }
    });
    return result;
}

function isInArray(data, key, search, forAllCharacters){
    return searchInArray(data, key, search, forAllCharacters).length > 0 ? true : false;
}

function removeFromArray(array, fieldSearch, fieldSearchValue) {
    var search = searchInArray(array, fieldSearch, fieldSearchValue, true);
    jQuery.each(search, function( index, value ) {
        var idx = array.indexOf(search[index]);
        if (idx !== -1) {
            array.splice(idx, 1);
        }
    });
    return array;
}

function isEmpty(str) {
    return (!str || 0 === str.length);
}
/**
 * Created by ch3wbi on 05/03/2017.
 */
(function($){

    "use strict";

    $(document).ready(function () {

        /*$('.nav-tabs a').click(function() {
            $('.nav-tabs li').removeClass('active');
            $('.tab-content').find('.tab-pane').removeClass('active in');
            $($(this).attr('href')).addClass('active in');
            $(this).closest('li').addClass('active');

            return false;
        });*/

        var gamesLeft = [];
        function myMethod()
        {
            var currentIndex = $('#ng_games_left_bar').data('indexleague');
            if(isNaN(parseInt(currentIndex))){
                currentIndex = -1;
            }
            currentIndex++;
            if(!gamesLeft[currentIndex]){
                currentIndex = 0;
            }
            $('#ng_games_left_bar').data('indexleague', currentIndex);

            var pourc = ((gamesLeft[currentIndex].totalGames - gamesLeft[currentIndex].nbGamesLeft) / gamesLeft[currentIndex].totalGames ) * 100;
            $('#ng_games_left_bar progress').attr('value', pourc);
            var txt = Math.floor(pourc)+'%';

            //$('#ng_games_left_bar progress').text(txt);
            $('#ng_games_left_bar div').html(txt);
            $('#ng_games_left_bar p').html(gamesLeft[currentIndex].league_name + ' ('+ gamesLeft[currentIndex].gametype + ') ');
        }

        $.ajax({
            type: 'POST',
            url: Routing.generate('profideo_league_nb_games_left'),
            data: {
                "id": $('#user-infos').data('user')
            },
            success: function (data) {
                if(data.result.length > 0){
                    gamesLeft = data.result;
                    myMethod();
                    if(data.result.length > 1){
                        setInterval(myMethod, 5000);
                    }
                }else{
                    $('#ng_games_left_bar').hide();
                }
            }
        });



        $('.btn-generate').click(function(){
            var game_type_id = $(this).data('gametype');

            var dialog = bootbox.dialog({
                message: "Veuillez patienter...",
                closeButton: false
            });

            $.ajax({
                type: 'POST',
                url: Routing.generate('profideo_league_generate'),
                data: {
                    "season": $(this).data('season'),
                    "game_type": game_type_id,
                },
                success: function (data) {
                    //console.log(data.games);
                    //console.log(data.games);
                    if(data.games.length > 0){
                        dialog.modal('hide');
                        location.reload();
                    }
                }
            });
        });

        $('.game_done').click(function(){
            var game_id = $(this).data('game');
            var score_home = $('#scoreHome_' + game_id).val();
            var score_visitor = $('#scoreVisitor_' + game_id).val();
            $.ajax({
                type: 'POST',
                url: Routing.generate('profideo_league_game_done'),
                data: {
                    "game_id": game_id,
                    "score_home": score_home,
                    "score_visitor": score_visitor
                },
                success: function (data) {
                    $('#game_' + game_id).remove();
                    //var $li = $('<li>' + data.userHome + ' ' + score_home + ' - ' + score_visitor + ' ' + data.userVisitor + '</li>');
                    //console.log($('#game_' + game_id));
                   // $('#calendarIsDone').append($('#game_' + game_id));

                    //Todo refresh calendars
                    refreshCalendars(data.league_id);
                    refreshClassement(data.season_id, data.league_id, 'classement_' + data.season_id + '_' + data.league_id);
                }
            });

        });

        function refreshCalendars(league_id){
            //calendarToPlay
            //calendarIsDone
            $.ajax({
                type: 'POST',
                url: Routing.generate('profideo_league_calendar'),
                data: {
                    "isdone": true,
                    "league": league_id
                },
                success: function (data) {
                    $('#calendarIsDone').html(data);
                }
            });
        }
        function refreshClassement(season_id, league_id, target_id){
            $.ajax({
                type: 'POST',
                url: Routing.generate('profideo_league_refresh_classement'),
                data: {
                    "season": season_id,
                    "league": league_id
                },
                success: function (data) {
                    $('#' + target_id).empty();
                    $('#' + target_id).html(data);
                }
            });
        }


    });

})(jQuery);

<?php

namespace ProfideoLeagueBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ProfideoLeagueBundle\Entity\Game;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GameController extends Controller
{
    public function gamesAction()
    {
        return $this->render('ProfideoLeagueBundle:Game:games.html.twig');
    }

    public function gameDoneAction(Request $request){

        $em = $this->getDoctrine()->getManager();

        $game_id = $request->get("game_id");
        $score_home = $request->get("score_home");
        $score_visitor = $request->get("score_visitor");

        $gameRepository = $em->getRepository('ProfideoLeagueBundle:Game');
        $game = $gameRepository->find($game_id);

        $game->setScoreHome($score_home);
        $game->setScoreVisitor($score_visitor);
        $game->setIsDone(true);
        $em->persist($game);
        $em->flush();

        return new JsonResponse(
            [
                'season_id' => $game->getSeason()->getId(),
                'league_id' => $game->getLeague()->getId(),
                'userHome' => $game->getUserHome()->getFullname(),
                'userVisitor' => $game->getUserVisitor()->getFullname(),
            ]);
    }

    public function calendarAction(Request $request){

        $em = $this->getDoctrine()->getManager();

        $isDone = $request->get("isdone");
        //$gameTypeId = $request->get("gametype");
        $leagueId = $request->get("league");

        //$gameRepository = $em->getRepository('ProfideoLeagueBundle:Game');
        //$gameTypeRepository = $em->getRepository('ProfideoLeagueBundle:GameType');
        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $league = $leagueRepository->find($leagueId);

        //gameType.getCurrentSeason().getGamesByLeague(league.id)

        $games = [];
        if($league){
            $gameType = $league->getGameType();
            $currentSeason = $gameType->getCurrentSeason();
            if($currentSeason){
                $games = $currentSeason->getGamesByLeague($league->getId());
            }
        }

        return $this->render('ProfideoLeagueBundle:Game:calendar.html.twig',
            array(
                'games' => $games,
                'isDone' => $isDone
            ));
    }

}

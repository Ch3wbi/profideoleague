<?php

namespace ProfideoLeagueBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use ProfideoLeagueBundle\Entity\Game;
use ProfideoLeagueBundle\Entity\Season;
use ProfideoLeagueBundle\Entity\League;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Doctrine\Common\Collections\Criteria;

class LeagueController extends Controller
{
    /**
     * Matches /classement
     *
     * @Route("/classement", name="profideo_league_classement")
     */
    public function classementAction($season_id, $league_id){
        $em = $this->getDoctrine()->getManager();

        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $league = $leagueRepository->findOneBy(array('id' => $league_id));

        $classement = $this->getClassement($season_id, $league);

        return $this->render('ProfideoLeagueBundle:Game:classement.html.twig',
            array(
                'league' => $league,
                'users' => $classement));
    }

    public function refreshClassementAction(Request $request){
        $season_id = $request->get("season");
        $league_id = $request->get("league");

        $em = $this->getDoctrine()->getManager();

        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $league = $leagueRepository->findOneBy(array('id' => $league_id));

        $classement = $this->getClassement($season_id, $league);


        return $this->render('ProfideoLeagueBundle:Game:classement.html.twig',
            array(
                'league' => $league,
                'users' => $classement));
    }

    public function getClassement($season_id, $league, $sort = ''){
        $result = [];

        $em = $this->getDoctrine()->getManager();

        foreach($league->getUsers() as $user){
            $gameRepository = $em->getRepository('ProfideoLeagueBundle:Game');
            $games = $gameRepository->getGamesByUserBySeasonByLeague($user->getId(), $league->getId(), $season_id);

            //si aucun match pour le joueur, c'est qu'il a été ajouté après une génération, donc il ne peut
            //être pris en compte dans le classement
            if(count($games) > 0){

                $pts = 0;
                $game_played = 0;
                $g = 0;
                $n = 0;
                $p = 0;
                $bp = 0;
                $bc = 0;
                $diff = 0;
                foreach ( $games as $game) {
                    if($game->getIsDone()){
                        $game_played++;
                        if($game->getUserHome()->getId() == $user->getId()){
                            if($game->getScoreHome() > $game->getScoreVisitor()) $g++;
                            if($game->getScoreHome() == $game->getScoreVisitor()) $n++;
                            if($game->getScoreHome() < $game->getScoreVisitor()) $p++;
                            $bp+= $game->getScoreHome();
                            $bc+= $game->getScoreVisitor();
                        }
                        if($game->getUserVisitor()->getId() == $user->getId()){
                            if($game->getScoreVisitor() > $game->getScoreHome()) $g++;
                            if($game->getScoreVisitor() == $game->getScoreHome()) $n++;
                            if($game->getScoreVisitor() < $game->getScoreHome()) $p++;
                            $bp+= $game->getScoreVisitor();
                            $bc+= $game->getScoreHome();
                        }
                    }
                }
                $pts = ($g * 3) + $n;
                $diff = $bp - $bc;

                $classement = new \stdClass();
                $classement->userId = $user->getId();
                $classement->user = $user;
                $classement->pts = $pts;
                $classement->game_played = $game_played;
                $classement->g = $g;
                $classement->n = $n;
                $classement->p = $p;
                $classement->bp = $bp;
                $classement->bc = $bc;
                $classement->diff = $diff;
                array_push($result, $classement);
                //echo "pts: " . $pts. "Matchs joués: ".$game_played." G:". $ng.' N: '.$nn.' P: '.$np. " bp:". $bp.' bc: '.$bc.' diff: '.$diff;
                //die;

            }

        }

        if($sort == 'DESC'){
            usort($result,function($a,$b){
                setlocale (LC_COLLATE, 'pl_PL.UTF-8'); // Example of Polish language collation
                if($b->pts == $a->pts) {
                    if($b->diff == $a->diff){
                        return $b->bp < $a->bp;
                    }else{
                        return $b->diff < $a->diff;
                    }
                }else{
                    return $b->pts < $a->pts;
                }
            });
        }else{
            usort($result,function($a,$b){
                setlocale (LC_COLLATE, 'pl_PL.UTF-8'); // Example of Polish language collation
                if($b->pts == $a->pts) {
                    if($b->diff == $a->diff){
                        return $b->bp > $a->bp;
                    }else{
                        return $b->diff > $a->diff;
                    }
                }else{
                    return $b->pts > $a->pts;
                }
            });
        }


        return $result;
    }



    public function pushGame($league, $users, $keyHome, $key_visitor){
        if(array_key_exists($key_visitor, $users)){
            $game = new \stdClass();
            $game->home = $users[$keyHome];
            $game->visitor = $users[$key_visitor];
            $game->league = $league->getId();

            return $game;
        }else{
            return null;
        }
    }


    public function toto($games, $game){
        foreach($games as $d){
            if( ($d->home == $game->home && $d->visitor == $game->visitor) ||
                ($d->home == $game->visitor && $d->visitor == $game->home) ){
                return true;
            }
        }
        return false;
    }



    public function generateAction(Request $request){

        $generate = true;

        $em = $this->getDoctrine()->getManager();

        $season_id = $request->get("season");
        $gameType_id = $request->get("game_type");
        $next_season = $request->get("next");

        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $userRepository = $em->getRepository('ProfideoLeagueBundle:User');

        $gametypeRepository = $em->getRepository('ProfideoLeagueBundle:GameType');
        $gameType = $gametypeRepository->findOneBy(array('id' => $gameType_id));

        $seasonRepository = $em->getRepository('ProfideoLeagueBundle:Season');
        $season = $seasonRepository->findOneBy(array('id' => $season_id));

        if(!$season){ //première saison
            $season = new Season();
            $season->setName('Saison 1');
            $season->setNumber(1);
            $season->setGameType($gameType);
            $em->persist($season);
            $em->flush();
        }else if($season->is_done()){ //creation nouvelle saison
            $previous_saison = $season;
            $season_number = $previous_saison->getNumber() + 1;
            $season = new Season();
            $season->setName('Saison ' . $season_number);
            $season->setNumber($season_number);
            $season->setGameType($gameType);
            $em->persist($season);
            $em->flush();
        }

        /*$season->setGames([]);
        $em->flush();*/
        //on supprime les matchs si il en existe
        foreach($season->getGames() as $game){
            $em->remove($game);
            $em->flush();
        }

        /*$leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $leagues = $leagueRepository->findAll();*/

        //descente ou montée des joueurs par rapport à la saison prédédente
        $sort = Criteria::create();
        $sort->orderBy(Array(
            'level' => Criteria::ASC
        ));
        $classement = [];
        $leagues = $gameType->getLeagues()->matching($sort);
        foreach($leagues as $league){
            if($league->getNumberToUpgrade() > 0){ //montée
                //récupère la league au dessus
                $leagueUp = $leagueRepository->findOneBy(array('gameType' => $league->getGameType()->getId(), 'level' => $league->getLevel() - 1));
                if($leagueUp){
                    $classement = $this->getClassement($season_id, $league);
                    $indexUser = 0;
                    foreach($classement as $c){
                        if($indexUser < $league->getNumberToUpgrade()){
                            $userToUp = $userRepository->findOneBy(array('id' => $c->userId));
                            $userToUp->removeLeague($league);
                            $userToUp->addLeague($leagueUp);
                            //$userToUp->setLeague($leagueUp);
                            $em->persist($userToUp);
                            $em->flush();
                            $indexUser++;
                        }
                    }
                }
            }
            if($league->getNumberToDowngrade() > 0){ //descente
                //récupère la league en dessous
                $leagueDown = $leagueRepository->findOneBy(array('gameType' => $league->getGameType()->getId(), 'level' => $league->getLevel() + 1));
                if($leagueDown){
                    $classement = $this->getClassement($season_id, $league, 'DESC');
                    $indexUser = 0;
                    foreach($classement as $c){
                        if($indexUser < $league->getNumberToDowngrade()){
                            $userToDown = $userRepository->findOneBy(array('id' => $c->userId));
                            $userToDown->removeLeague($league);
                            $userToDown->addLeague($leagueDown);
                            //$userToDown->setLeague($leagueDown);
                            $em->persist($userToDown);
                            $em->flush();
                            $indexUser++;
                        }
                    }
                }
            }
        }

        $games = [];

        //todo en attendant de rafraichir la liste des users
        //$gameType = $gametypeRepository->findOneBy(array('id' => $gameType_id));

        foreach($gameType->getLeagues() as $league){
            //$users2 = $userRepository->getUsersByLeague($league->getId());
            $users2 = $userRepository->getUsersByLeague($league->getId());
            /*$leagueRefresh = $leagueRepository->find($league->getId());
            $users2 = $leagueRefresh->getUsers();*/

            $users = [];
            foreach ( $users2 as $user) {
                //if($user->getIsActived()) array_push($users, $user->getId());
                array_push($users, $user->getId());
            }
            shuffle($users);

            foreach ( $users as $user) {
                $key = array_search($user, $users);

                $countHomeForUser = 0;

                for($i = 0 ; $i <= $league->getNbDays() ; $i++){

                    //$key_visitor = ($key_visitor == $i) ? ($key_visitor + 1) : $key_visitor;
                    $key_visitor = $i;

                    /*if($countHomeForUser >= $league->getNbMaxHome()){
                        $game = $this->pushGame($league, $users, $key_visitor, $key);
                        //echo "home: ".$users[$key_visitor]. '- visitor: '.$users[$key].'<br>';
                    }else{
                        $game = $this->pushGame($league, $users, $key, $key_visitor);
                        //echo "home: ".$users[$key]. '- visitor: '.$users[$key_visitor].'<br>';
                    }*/
                    $game = $this->pushGame($league, $users, $key, $key_visitor);

                    if(isset($users[$key]) && isset($users[$key_visitor])){
                        if($users[$key] != $users[$key_visitor] && !$this->toto($games, $game) && $countHomeForUser <= $league->getNbMaxHome()){
                            //echo $key_visitor.'<br>';
                            //echo "home: ".$users[$key]. '- visitor: '.$users[$key_visitor].'<br>';

                            $g = new Game();
                            $g->setLeague($league);
                            $g->setSeason($season);



                            $userRepository = $em->getRepository('ProfideoLeagueBundle:User');
                            $user = $userRepository->find($game->home);
                            $g->setUserHome($user);
                            $user = $userRepository->find($game->visitor);
                            $g->setUserVisitor($user);
                            $em->persist($g);
                            $em->flush();


                            /*return new JsonResponse(
                                [
                                    'games' => $game->home.' ' .$game->visitor.' '.$g->getScoreHome().' '.$g->getId()
                                ]);*/

                            array_push($games, $game);
                            $countHomeForUser++;
                        }
                    }



                    //$key_visitor++;

                }
            }
        }




        return new JsonResponse(
            [
                'games' => $games
            ]);
    }



    /**
     * Matches /leagues
     *
     * @Route("/leagues", name="profideo_league_leagues")
     */
    public function leaguesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $leagues = $leagueRepository->getLeagues();

        return new JsonResponse(
            [
                'leagues' => $leagues
            ]);
    }

    public function leagueEditAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $league_id = $request->get("id");
        $name = $request->get("name");
        $level = $request->get("level");
        $numberToUpgrade = $request->get("number_to_upgrade");
        $numberToDowngrade = $request->get("number_to_downgrade");
        $gametype_id = $request->get("gametype_id");

        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $league = $leagueRepository->findOneBy(array('id' => $league_id));

        $result = null;
        if(!$league_id){
            $league = new League();
        }
        $league->setName($name);
        $league->setLevel($level);
        $league->setNumberToUpgrade($numberToUpgrade);
        $league->setNumberToDowngrade($numberToDowngrade);

        $gameTypeRepository = $em->getRepository('ProfideoLeagueBundle:GameType');
        $gametype = $gameTypeRepository->findOneBy(array('id' => $gametype_id));
        if($gametype){
            $league->setGameType($gametype);
        }


        $em->persist($league);
        $em->flush();
        $result = $league->tostdclass();
        return new JsonResponse(
            [
                'result' => $result
            ]);
    }

    public function leagueRemoveAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $league_id = $request->get("id");

        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $league = $leagueRepository->findOneBy(array('id' => $league_id));

        $result = false;
        if($league){
            if(count($league->getUsers()) == 0){
                $em->remove($league);
                $em->flush();
                $result = true;
            }
        }
        return new JsonResponse(
            [
                'result' => $result
            ]);
    }

}

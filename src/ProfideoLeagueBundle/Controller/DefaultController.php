<?php

namespace ProfideoLeagueBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ProfideoLeagueBundle\Entity\League;
use ProfideoLeagueBundle\Entity\Game;
use ProfideoLeagueBundle\Entity\Season;
use ProfideoLeagueBundle\Entity\User;
use Symfony\Component\Security\Core\SecurityContext;
use Doctrine\Common\Collections\Criteria;

class DefaultController extends Controller
{
    function array_equal($a, $b) {
        return (
            is_array($a) && is_array($b) &&
            count($a) == count($b) &&
            array_diff($a, $b) === array_diff($b, $a)
        );
    }

    public function loginAction(Request $request)
    {
        $securityContext = $this->container->get('security.authorization_checker');
        // Si le visiteur est déjà identifié, on le redirige vers l'accueil
        if ($securityContext->isGranted('IS_AUTHENTICATED_REMEMBERED')) {
            return $this->redirectToRoute('profideo_league_homepage');
        }

        // Le service authentication_utils permet de récupérer le nom d'utilisateur
        // et l'erreur dans le cas où le formulaire a déjà été soumis mais était invalide
        // (mauvais mot de passe par exemple)
        $authenticationUtils = $this->get('security.authentication_utils');

        return $this->render('ProfideoLeagueBundle:Default:login.html.twig', array(
            'last_username' => $authenticationUtils->getLastUsername(),
            'error'         => $authenticationUtils->getLastAuthenticationError(),
        ));
    }

    public function getAvatarAction(Request $request){


        //Si on tente d'accéder à l'action sans passer par une requete ajax, on renvoit une page d'erreur
        if (!$request->isXmlHttpRequest()) {
            throw $this->createNotFoundException();
        }

        $em = $this->getDoctrine()->getManager();

        $username = $request->get("username");

        $userRepository = $em->getRepository('ProfideoLeagueBundle:User');
        $user = $userRepository->findOneByUsername($username);

        if($user){
            return new JsonResponse(
                [
                    'gravatar'=> $user->gravatar()
                ]);
        }else{
            return new JsonResponse();
        }
    }

    /**
     * Matches /
     *
     * @Route("/", name="profideo_league_homepage")
     */
    public function indexAction()
    {


        //$this->get('doctrine');
        $em = $this->getDoctrine()->getManager();

        //a ne pas effacer, c'est pour donner un mot de passe a chaque user
        /*$userRepository = $em->getRepository('ProfideoLeagueBundle:User');
        $users = $userRepository->findAll();

        foreach ( $users as $user) {
            $encoder = $this->get('security.encoder_factory')->getEncoder($user);
            $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
            $user->setPassword($encodedPass);

            $em->persist($user);
            $em->flush();
        }*/

        $gameTypeRepository = $em->getRepository('ProfideoLeagueBundle:GameType');
        $gameTypes = $gameTypeRepository->findAll(array('id' => 'ASC'));

        //$seasonRepository = $em->getRepository('ProfideoLeagueBundle:Season');
        //$season = $seasonRepository->getLast($gameTypes[1]->getId());

        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $leagues = $leagueRepository->findAll();

        /*$total_matchs = 0;
        foreach ( $leagues as $league) {
            //echo "nb day: ".$league->getNbDays();
            //echo "<br>";
            //echo "nb total de matchs: ".$league->getNbTotalGame();
            $total_matchs+=$league->getNbTotalGame();
        }

        $no_games_have_been_played = true;

        //Si la saison comporte le bon nombre de matchs possible et qu'ils ont tous été jouer, on ne laisse pas la possibilité de regénérer
        if(count($season->getGames()) == $total_matchs) {
            foreach ($season->getGames() as $game) {
                if ($game->getIsDone()) {
                    $no_games_have_been_played = false;
                }
            }
        }*/




        /*
         *
         * $query = $this->getEntityManager()->createQuery(
            'SELECT s FROM prodideoLeagueBundle:Season s WHERE s.number = (SELECT max(s2.number) FROM prodideoLeagueBundle:Season s2)'
        );

        return $result = $query->getOneOrNullResult();
         */
        return $this->render('ProfideoLeagueBundle:Default:index.html.twig', array(
            'gameTypes' => $gameTypes,
            //'season' => $season,
            'leagues' => $leagues
        ));
    }

    /**
     * Matches /navigation
     *
     * @Route("/navigation", name="profideo_league_navigation")
     */
    public function navigationAction(){

        $em = $this->getDoctrine()->getManager();

        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $leagues = $leagueRepository->findAll(array('level' => 'ASC'));

        return $this->render('ProfideoLeagueBundle::navigation.html.twig',
            array(
                'leagues' => $leagues));

    }

}

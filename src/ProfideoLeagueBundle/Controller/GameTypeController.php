<?php

namespace ProfideoLeagueBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ProfideoLeagueBundle\Entity\League;
use ProfideoLeagueBundle\Entity\Game;
use ProfideoLeagueBundle\Entity\Season;
use ProfideoLeagueBundle\Entity\User;
use ProfideoLeagueBundle\Entity\GameType;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

class GameTypeController extends Controller
{
    /**
     * Matches /gametypes
     *
     * @Route("/gametypes", name="profideo_league_gametypes")
     */
    public function gametypesAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $gameTypeRepository = $em->getRepository('ProfideoLeagueBundle:GameType');
        $gametypes = $gameTypeRepository->getGameTypes();

        return new JsonResponse(
            [
                'gametypes' => $gametypes
            ]);
    }

    public function gametypeEditAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $gametype_id = $request->get("id");
        $label = $request->get("label");

        $gameTypeRepository = $em->getRepository('ProfideoLeagueBundle:GameType');
        $gametype = $gameTypeRepository->findOneBy(array('id' => $gametype_id));

        $result = null;
        if(!$gametype){
            $gametype = new GameType();
        }
        $gametype->setLabel($label);

        $em->persist($gametype);
        $em->flush();

        $result = new \stdClass();
        $result->id = $gametype->getId();
        $result->label = $gametype->getLabel();

        return new JsonResponse(
            [
                'result' => $result
            ]);
    }

    public function gametypeRemoveAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $gametype_id = $request->get("id");

        $gameTypeRepository = $em->getRepository('ProfideoLeagueBundle:GameType');
        $gametype = $gameTypeRepository->findOneBy(array('id' => $gametype_id));

        $result = false;
        if($gametype){
            if(count($gametype->getLeagues()) == 0 && count($gametype->getSeasons()) == 0){
                $em->remove($gametype);
                $em->flush();
                $result = true;
            }
        }
        return new JsonResponse(
            [
                'result' => $result
            ]);
    }

}

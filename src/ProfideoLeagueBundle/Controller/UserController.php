<?php

namespace ProfideoLeagueBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ProfideoLeagueBundle\Entity\League;
use ProfideoLeagueBundle\Entity\Game;
use ProfideoLeagueBundle\Entity\Season;
use ProfideoLeagueBundle\Entity\User;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Doctrine\Common\Collections\ArrayCollection;
use ProfideoLeagueBundle\Form\ProfilType;

class UserController extends Controller
{
    /**
     * Matches /users_list
     *
     * @Route("/users_list", name="profideo_league_users_list")
     */
    public function usersListAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $userRepository = $em->getRepository('ProfideoLeagueBundle:User');
        $users = $userRepository->findAll(); //getUsers();

        $result = [];
        foreach($users as $user){
            array_push($result, $user->tostdclass());
        }
        /*$encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $jsonContent = $serializer->serialize($users, 'json');*/


        return new JsonResponse(
            [
                'users' => $result
            ]);
    }

    public function nbGamesLeftAction(Request $request){
        $result = [];

        $user_id = $request->get("id");

        $em = $this->getDoctrine()->getManager();
        $userRepository = $em->getRepository('ProfideoLeagueBundle:User');
        $user = $userRepository->findOneBy(array('id' => $user_id));

        if($user){
            $gameRepository = $em->getRepository('ProfideoLeagueBundle:Game');
            foreach($user->getLeagues() as $league){
                $tmp = new \stdClass();
                $games = $gameRepository->getGamesByUserBySeasonByLeague($user, $league, $league->getGameType()->getCurrentSeason());
                $nbGamesLeft = 0;
                $totalGames = 0;
                foreach($games as $game){
                    if($game->getIsDone() == false) $nbGamesLeft++;
                    $totalGames++;
                }
                $tmp->totalGames = $totalGames;
                $tmp->nbGamesLeft = $nbGamesLeft;
                $tmp->league_name = $league->getName();
                $tmp->league = $league->getId();
                $tmp->gametype = $league->getGameType()->getLabel();
                array_push($result, $tmp);
            }
        }

        return new JsonResponse(
            [
                'result' => $result
            ]);
    }

    public function sendMailsWelcomeAction(Request $request){
        $em = $this->getDoctrine()->getManager();

        $userRepository = $em->getRepository('ProfideoLeagueBundle:User');
        $users = $userRepository->findAll();

        foreach($users as $user){
            $this->sendMailWelcome($user);
        }

        return new JsonResponse(
            [
                'result' => 'ok'
            ]);
    }

    public function userEditAction(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $user_id = $request->get("id");
        $username = $request->get("username");
        $lastname = $request->get("lastname");
        $firsname = $request->get("firstname");
        $email = $request->get("email");
        $leagues = $request->get("leagues");
        $admin = $request->get("admin");

        //$leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        //$league = $leagueRepository->findOneBy(array('id' => $league));

        $userRepository = $em->getRepository('ProfideoLeagueBundle:User');
        $user = $userRepository->findOneBy(array('id' => $user_id));

        $result = null;

        $new = false;
        if(!$user){
            $user = new User();
            $new = true;
        }
        $user->setUsername($username);
        $user->setLastname($lastname);
        $user->setFirstname($firsname);
        $user->setEmail($email);
        $user->setAvatar("");

        $newleagues = new ArrayCollection();
        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        if($leagues){
            foreach($leagues as $l){
                $league = $leagueRepository->findOneBy(array('id' => $l));
                if($league){
                    $newleagues[] = $league;
                }
            }
        }

        $user->setLeagues($newleagues);

        $user->setSalt(md5($user->getUsername()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
        $user->setPassword($encodedPass);

        //if($league) $user->setLeague($league);
        if($admin) $user->setRoles('ROLE_ADMIN'); else $user->setRoles('ROLE_USER');

        $em->persist($user);
        $em->flush();
        $result = $user->tostdclass();

        if($new){
            $image = $this->container->getParameter('kernel.root_dir').'/../web/bundles/profideoleague/img/logos/logo-profideo-league.png';
            //$image = $message->embed(Swift_Image::fromPath('/bundles/profideoleague/img/logos/logo-profideo-league.png'));
            $message = \Swift_Message::newInstance()
                ->setSubject('Bienvenue sur Profideo Leagues')
                //->setFrom($email)
                ->setFrom($this->container->getParameter('sender_address'))
                ->setTo($user->getEmail());

                $message->setBody($this->renderView('ProfideoLeagueBundle:Utils:mail.html.twig', array(
                    'domaine' => $this->container->getParameter("domaine"),
                    'user' => $user,
                    'image' => $message->embed(\Swift_Image::fromPath($image))
                ))
                    ,'text/html');
            $this->get('mailer')->send($message);
        }

        return new JsonResponse(
            [
                'result' => $result
            ]);
    }

    public function sendMailWelcome($user){
        $image = $this->container->getParameter('kernel.root_dir').'/../web/bundles/profideoleague/img/logos/logo-profideo-league.png';
        //$image = $message->embed(Swift_Image::fromPath('/bundles/profideoleague/img/logos/logo-profideo-league.png'));
        $message = \Swift_Message::newInstance()
            ->setSubject('Bienvenue sur Profideo Leagues')
            //->setFrom($email)
            ->setFrom($this->container->getParameter('sender_address'))
            ->setTo($user->getEmail());

        $message->setBody($this->renderView('ProfideoLeagueBundle:Utils:mail.html.twig', array(
            'domaine' => $this->container->getParameter("domaine"),
            'user' => $user,
            'image' => $message->embed(\Swift_Image::fromPath($image))
        ))
            ,'text/html');
        $this->get('mailer')->send($message);
    }

    public function userRemoveAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $user_id = $request->get("id");

        $userRepository = $em->getRepository('ProfideoLeagueBundle:User');
        $user = $userRepository->findOneBy(array('id' => $user_id));

        $result = null;
        if($user){
            $user->setIsActived($user->getIsActived() ? false : true);
            $em->persist($user);
            $em->flush();
            $result = $user->tostdclass();
        }
        return new JsonResponse(
            [
                'result' => $result
            ]);
    }

    /**
     * Matches /profil
     *
     * @Route("/profil", name="profideo_league_profil")
     */
    public function profilAction(Request $request)
    {
        if($this->getUser()){

            $user = $this->getUser();
            //$image = $user->getAvatar();
            $imageDir = $this->container->getParameter('kernel.root_dir').'/../web/users/';

            $currentAvatar = '';
            if($dir=opendir($imageDir)) {
                while ($element = readdir($dir)) {
                    if ($element != '.' && $element != '..') {
                        $info = new \SplFileInfo($imageDir.'/'.$element);
                        $ext = $info->getExtension();
                        if(basename($element,'.'.$ext) == $user->getUsername()) {
                            $currentAvatar = $element;
                            break;
                        }
                    }
                }

                closedir($dir);
            }
            //$form = $this->createForm(new ProfilType(), $user, array('attr' => array()));
            $form = $this->createForm(ProfilType::class, $user);
            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()){
                $em = $this->getDoctrine()->getManager();

                $file = $user->getAvatar();
                if($file){
                    $filename = $user->getUsername().'.'.$file->guessExtension();
                    $file->move($imageDir, $filename);

                    $user->setAvatar($filename);
                }else{
                    $user->setAvatar($currentAvatar);
                }



                $user->setSalt(md5($user->getPassword()));
                $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
                $encodedPass = $encoder->encodePassword($user->getPassword(), $user->getSalt());
                $user->setPassword($encodedPass);



                /*
                $rawPassword = $user->getRawPassword();

                $encoder = $this->get('security.password_encoder'); // encode the password
                $password = $encoder->encodePassword($user, $rawPassword);
                $this->getUser()->setPassword($password);
*/
                $em->persist($user);
                $em->flush();
            }
            return $this->render('ProfideoLeagueBundle:User:profil.html.twig',
                array('form' => $form->createView(), 'user' => $user));
        }else{
            $this->get('session')->getFlashBag()->add('warning', "Vous n'êtes pas autoriser à accéder à cette page !");
            return new RedirectResponse($this->router->generate('profideo_league_homepage'));
        }
    }

    public function removeAvatarAction(Request $request){
        $em = $this->getDoctrine()->getManager();
        $user_id = $request->get("id");

        $user = $this->getUser();
        $imageDir = $this->container->getParameter('kernel.root_dir').'/../web/users/';

        if(file_exists($imageDir.'/'.$user->getAvatar())){
            unlink($imageDir.'/'.$user->getAvatar());
        }
        $user->setAvatar('');
        $em->persist($user);
        $em->flush();

        return new JsonResponse(
            [
                'result' => ''
            ]);
    }

}

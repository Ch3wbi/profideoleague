<?php

namespace ProfideoLeagueBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ProfideoLeagueBundle\Entity\League;
use ProfideoLeagueBundle\Entity\Game;
use ProfideoLeagueBundle\Entity\Season;
use ProfideoLeagueBundle\Entity\User;


class StatistiquesController extends Controller
{
    /**
     * Matches /statistiques/index
     *
     * @Route("/statistiques/index", name="profideo_league_statistiques_index")
     */
    public function indexAction()
    {

        //$this->get('doctrine');
        $em = $this->getDoctrine()->getManager();

        $gameTypeRepository = $em->getRepository('ProfideoLeagueBundle:GameType');
        $gameTypes = $gameTypeRepository->findAll(array('id' => 'ASC'));

        /*$seasonRepository = $em->getRepository('ProfideoLeagueBundle:Season');
        $season = $seasonRepository->getLast();*/

        $seasonRepository = $em->getRepository('ProfideoLeagueBundle:Season');
        $seasons = $seasonRepository->findAll(array('number' => 'ASC'));

        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $leagues = $leagueRepository->findAll(array('level' => 'ASC'));


        return $this->render('ProfideoLeagueBundle:Statistiques:index.html.twig', array(
            'gameTypes' => $gameTypes,
            //'season' => $season,
            'seasons' => $seasons,
            'leagues' => $leagues
        ));
    }

    public function butsAction(Request $request){
        $result = [];

        $season_id = $request->get("season_id");
        $league_id = $request->get("league_id");

        $em = $this->getDoctrine()->getManager();

        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $league = $leagueRepository->findOneBy(array('id' => $league_id));

        foreach($league->getUsers() as $user){
            $gameRepository = $em->getRepository('ProfideoLeagueBundle:Game');
            $games = $gameRepository->getGamesByUserBySeasonByLeague($user->getId(), $league->getId(), $season_id);

            $pts = 0;
            $game_played = 0;
            $g = 0;
            $n = 0;
            $p = 0;
            $bp = 0;
            $bc = 0;
            $diff = 0;
            foreach ( $games as $game) {
                if($game->getIsDone()){
                    $game_played++;
                    if($game->getUserHome()->getId() == $user->getId()){
                        if($game->getScoreHome() > $game->getScoreVisitor()) $g++;
                        if($game->getScoreHome() == $game->getScoreVisitor()) $n++;
                        if($game->getScoreHome() < $game->getScoreVisitor()) $p++;
                        $bp+= $game->getScoreHome();
                        $bc+= $game->getScoreVisitor();
                    }
                    if($game->getUserVisitor()->getId() == $user->getId()){
                        if($game->getScoreVisitor() > $game->getScoreHome()) $g++;
                        if($game->getScoreVisitor() == $game->getScoreHome()) $n++;
                        if($game->getScoreVisitor() < $game->getScoreHome()) $p++;
                        $bp+= $game->getScoreVisitor();
                        $bc+= $game->getScoreHome();
                    }
                }
            }
            $pts = ($g * 3) + $n;
            $diff = $bp - $bc;

            $classement = new \stdClass();
            $classement->user_fullname = $user->getFullname();
            $classement->pts = $pts;
            $classement->game_played = $game_played;
            $classement->g = $g;
            $classement->n = $n;
            $classement->p = $p;
            $classement->bp = $bp;
            $classement->bc = $bc;
            $classement->diff = $diff;
            array_push($result, $classement);
            //echo "pts: " . $pts. "Matchs joués: ".$game_played." G:". $ng.' N: '.$nn.' P: '.$np. " bp:". $bp.' bc: '.$bc.' diff: '.$diff;
            //die;
        }


        usort($result,function($a,$b){
            setlocale (LC_COLLATE, 'pl_PL.UTF-8'); // Example of Polish language collation
            if($b->pts == $a->pts) {
                if($b->diff == $a->diff){
                    return $b->bp > $a->bp;
                }else{
                    return $b->diff > $a->diff;
                }
            }else{
                return $b->pts > $a->pts;
            }
        });

        return new JsonResponse(
            [
                'users' => $result,
            ]);
    }


}

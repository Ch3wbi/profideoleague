<?php

namespace ProfideoLeagueBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use ProfideoLeagueBundle\Entity\League;
use ProfideoLeagueBundle\Entity\Game;
use ProfideoLeagueBundle\Entity\Season;
use ProfideoLeagueBundle\Entity\User;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class ParamsController extends Controller
{
    /**
     * Matches /params/leagues
     *
     * @Route("/params/leagues", name="profideo_league_params_leagues")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function paramsLeaguesAction()
    {


        //$this->get('doctrine');
        $em = $this->getDoctrine()->getManager();


        /*$seasonRepository = $em->getRepository('ProfideoLeagueBundle:Season');
        $season = $seasonRepository->getLast();*/

        $gameTypeRepository = $em->getRepository('ProfideoLeagueBundle:GameType');
        $gameTypes = $gameTypeRepository->findAll();

/*        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $leagues = $leagueRepository->findAll();*/


        return $this->render('ProfideoLeagueBundle:Params:leagues.html.twig', array(
            'gameTypes' => $gameTypes
        ));
    }

    /**
     * Matches /params/users
     *
     * @Route("/params/users", name="profideo_league_params_users")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function paramsUsersAction()
    {


        //$this->get('doctrine');
        $em = $this->getDoctrine()->getManager();

        /*$seasonRepository = $em->getRepository('ProfideoLeagueBundle:Season');
        $season = $seasonRepository->getLast();*/

        $gameTypeRepository = $em->getRepository('ProfideoLeagueBundle:GameType');
        $gameTypes = $gameTypeRepository->findAll();

        $leagueRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $leagues = $leagueRepository->findAll();

        $userRepository = $em->getRepository('ProfideoLeagueBundle:User');
        $users = $userRepository->findAll();



       /* $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());

        $serializer = new Serializer($normalizers, $encoders);

        $jsonContent = $serializer->serialize($users, 'json');*/

        return $this->render('ProfideoLeagueBundle:Params:users.html.twig', array(
            'gameTypes' => $gameTypes,
            'leagues' => $leagues,
            'users' => $users
        ));
    }

    /**
     * Matches /params/gametypes
     *
     * @Route("/params/gametypes", name="profideo_league_params_gametypes")
     * @Security("has_role('ROLE_ADMIN')")
     */
    public function paramsGameTypesAction()
    {


        //$this->get('doctrine');
        $em = $this->getDoctrine()->getManager();

        $gameTypeRepository = $em->getRepository('ProfideoLeagueBundle:League');
        $gameTypes = $gameTypeRepository->findAll();


        return $this->render('ProfideoLeagueBundle:Params:gametypes.html.twig', array(
            'gameTypes' => $gameTypes
        ));
    }

}

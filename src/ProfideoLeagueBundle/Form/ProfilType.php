<?php

namespace ProfideoLeagueBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\ORM\EntityRepository;

class ProfilType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('username', TextType::class, array('label' => 'Login', 'required' => true))
            ->add('lastname', TextType::class, array('label' => 'Nom', 'required' => true))
            ->add('firstname', TextType::class, array('label' => 'Prénom', 'required' => true))
            ->add('email', EmailType::class, array('label' => 'Email', 'required' => true))
            //->add('userType')
            /*->add('rawPassword', 'repeated', array(
                'type' => PasswordType::class,
                'invalid_message' => 'Les champs de mot de passe doivent correspondre !',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'first_options'  => array('label' => 'Mot de passe'),
                'second_options' => array('label' => 'Répéter le mot de passe'),
            ))*/
            ->add('password', RepeatedType::class, array(
                'type' => PasswordType::class,
                'invalid_message' => 'Les mots de passe ne correspondent pas.',
                'options' => array('attr' => array('class' => 'password-field')),
                'required' => true,
                'error_bubbling' => true,
                'first_options'  => array('label' => 'Mot de passe'),
                'second_options' => array('label' => 'Répéter le mot de passe'),
            ))
            ->add('avatar', FileType::class, array('data_class' => null, 'label' => 'Image de profil', 'required' => false))
            //->add('rawPassword', 'password', array('label' => 'Mot de passe', 'required' => true))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'ProfideoLeagueBundle\Entity\User',
            'attr' => ''
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'profideoleaguebundle_user';
    }
}

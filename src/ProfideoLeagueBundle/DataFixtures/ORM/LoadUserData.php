<?php
namespace ProfideoLeagueBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
//use Doctrine\Common\DataFixtures\AbstractFixture;
//use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use ProfideoLeagueBundle\Entity\User;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Symfony\Component\Security\Core\SecurityContext;

class LoadUserData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $em)
    {
        $user = new User();
        $user->setUsername('legrandfrere');
        $user->setLastname('Frère');
        $user->setFirstname('Le grand');
        $user->setEmail('pmagne@profideo.com');
        $user->setSalt(md5($user->getUsername()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
        $user->setPassword($encodedPass);
        $user->setRoles('ROLE_USER');
        $em->persist($user);
        $em->flush();

        $user = new User();
        $user->setUsername('med');
        $user->setLastname('med');
        $user->setFirstname('');
        $user->setEmail('machoubie@profideo.com');
        $user->setSalt(md5($user->getUsername()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
        $user->setPassword($encodedPass);
        $user->setRoles('ROLE_USER');
        $em->persist($user);
        $em->flush();

        $user = new User();
        $user->setUsername('fdegardin');
        $user->setLastname('Degardin');
        $user->setFirstname('Florent');
        $user->setEmail('fdegardin@profideo.com');
        $user->setSalt(md5($user->getUsername()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
        $user->setPassword($encodedPass);
        $user->setRoles('ROLE_USER');
        $em->persist($user);
        $em->flush();

        $user = new User();
        $user->setUsername('Nimo');
        $user->setLastname('Moulin');
        $user->setFirstname('Nicolas');
        $user->setEmail('nimo.moulin@gmail.com');
        $user->setSalt(md5($user->getUsername()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
        $user->setPassword($encodedPass);
        $user->setRoles('ROLE_ADMIN');
        $em->persist($user);
        $em->flush();

        $user = new User();
        $user->setUsername('isow');
        $user->setLastname('Sow');
        $user->setFirstname('Ibrahima');
        $user->setEmail('isow@profideo.com');
        $user->setSalt(md5($user->getUsername()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
        $user->setPassword($encodedPass);
        $user->setRoles('ROLE_USER');
        $em->persist($user);
        $em->flush();

        $user = new User();
        $user->setUsername('Meh10');
        $user->setLastname('Meh10');
        $user->setFirstname('');
        $user->setEmail('ebrahimi@profideo.com');
        $user->setSalt(md5($user->getUsername()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
        $user->setPassword($encodedPass);
        $user->setRoles('ROLE_USER');
        $em->persist($user);
        $em->flush();

        $user = new User();
        $user->setUsername('Benoit');
        $user->setLastname('Colombini');
        $user->setFirstname('Benoit');
        $user->setEmail('bcolombini@profideo.com');
        $user->setSalt(md5($user->getUsername()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
        $user->setPassword($encodedPass);
        $user->setRoles('ROLE_USER');
        $em->persist($user);
        $em->flush();

        $user = new User();
        $user->setUsername('LeRobin');
        $user->setLastname('LeRobin');
        $user->setFirstname('');
        $user->setEmail('rduval@profideo.com');
        $user->setSalt(md5($user->getUsername()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
        $user->setPassword($encodedPass);
        $user->setRoles('ROLE_USER');
        $em->persist($user);
        $em->flush();

        $user = new User();
        $user->setUsername('jmanu35');
        $user->setLastname('35');
        $user->setFirstname('Jean-Manu');
        $user->setEmail('jeollichon@profideo.com');
        $user->setSalt(md5($user->getUsername()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
        $user->setPassword($encodedPass);
        $user->setRoles('ROLE_USER');
        $em->persist($user);
        $em->flush();

        $user = new User();
        $user->setUsername('lPoldine');
        $user->setLastname('Poldine');
        $user->setFirstname('Leo');
        $user->setEmail('lmoreira@profideo.com');
        $user->setSalt(md5($user->getUsername()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
        $user->setPassword($encodedPass);
        $user->setRoles('ROLE_USER');
        $em->persist($user);
        $em->flush();

        $user = new User();
        $user->setUsername('omoulin');
        $user->setLastname('Moulin');
        $user->setFirstname('Olivier');
        $user->setEmail('moulin.oli@gmail.com');
        $user->setSalt(md5($user->getUsername()));
        $encoder = $this->container->get('security.encoder_factory')->getEncoder($user);
        $encodedPass = $encoder->encodePassword($user->getUsername(), $user->getSalt());
        $user->setPassword($encodedPass);
        $user->setRoles('ROLE_ADMIN');
        $em->persist($user);
        $em->flush();

    }

    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}

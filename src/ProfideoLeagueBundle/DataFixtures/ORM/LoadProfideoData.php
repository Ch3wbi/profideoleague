<?php
namespace ProfideoLeagueBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use ProfideoLeagueBundle\Entity\GameType;
use ProfideoLeagueBundle\Entity\League;
use ProfideoLeagueBundle\Entity\Season;

class LoadProfideoData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $em)
    {
        $gameTypeFifa = new GameType();
        $gameTypeFifa->setLabel('Fifa');
        $em->persist($gameTypeFifa);
        $em->flush();

        $gameTypebaby = new GameType();
        $gameTypebaby->setLabel('BabyFoot');
        $em->persist($gameTypebaby);
        $em->flush();

        $season = new Season();
        $season->setName('Saison 1');
        $season->setNumber(1);
        $season->setGameType($gameTypeFifa);
        $em->persist($season);
        $em->flush();

        $league = new League();
        $league->setName('Ligue 1');
        $league->setLevel(1);
        $league->setNumberToUpgrade(0);
        $league->setNumberToDowngrade(0);
        $league->setGameType($gameTypeFifa);
        $em->persist($league);
        $em->flush();

        $league = new League();
        $league->setName('Ligue 2');
        $league->setLevel(2);
        $league->setNumberToUpgrade(0);
        $league->setNumberToDowngrade(0);
        $league->setGameType($gameTypeFifa);
        $em->persist($league);
        $em->flush();

        $league = new League();
        $league->setName('D1');
        $league->setLevel(1);
        $league->setNumberToUpgrade(0);
        $league->setNumberToDowngrade(0);
        $league->setGameType($gameTypebaby);
        $em->persist($league);
        $em->flush();
    }

    public function getOrder()
    {
        return 1; // the order in which fixtures will be loaded
    }
}
